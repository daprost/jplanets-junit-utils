package de.jplanets.junit;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;

/**
 * Test the {@link AssertBigDecimal}.
 * 
 * @since 1.0.0
 *
 */
public class AssertBigDecimalTest {

    
    /**
     * Test that assertBigDecimalCompares_Stringnull_null no failure.
     */
    @Test
    public void assertBigDecimalCompares_Stringnull_null() {
    	AssertBigDecimal.assertBigDecimalCompares((String) null, null);
    }
    
    /**
     * Test that assertBigDecimalCompares_Stringnull_value raises failure.
     */
    @Test(expected = ComparisonFailure.class)
    public void assertBigDecimalCompares_Stringnull_value() {
    	AssertBigDecimal.assertBigDecimalCompares((String) null, BigDecimal.ONE);
    }
    
    /**
     * Test that assertBigDecimalCompares_value_null raises failure.
     */
    @Test(expected = ComparisonFailure.class)
    public void assertBigDecimalCompares_value_null() {
    	AssertBigDecimal.assertBigDecimalCompares("1", null);
    }
    
    /**
     * Test that assertBigDecimalCompares same no failure.
     */
    @Test
    public void assertBigDecimalCompares_same() {
    	AssertBigDecimal.assertBigDecimalCompares("0", BigDecimal.ZERO);
    	AssertBigDecimal.assertBigDecimalCompares("1.000", BigDecimal.ONE);
    	AssertBigDecimal.assertBigDecimalCompares("1E1", BigDecimal.TEN);
    	AssertBigDecimal.assertBigDecimalCompares("11E1", new BigDecimal("110.00"));
    }
    
    /**
     * Test that assertBigDecimalCompares different raises failure.
     */
    @Test
    public void assertBigDecimalCompares_different() {
        compareAndCatch("0.1", BigDecimal.ZERO);
        compareAndCatch("1.0000001", BigDecimal.ONE);
        compareAndCatch("9.99999999999999999", BigDecimal.TEN);
    }
    
    /**
     * Test that assertBigDecimalCompares_BDnull_null no failure.
     */
    @Test
    public void assertBigDecimalCompares_BDnull_null() {
    	AssertBigDecimal.assertBigDecimalCompares((BigDecimal) null, null);
    }
    
    /**
     * Test that assertBigDecimalCompares_BDnull_value raises failure.
     */
    @Test(expected = ComparisonFailure.class)
    public void assertBigDecimalCompares_BDnull_value() {
    	AssertBigDecimal.assertBigDecimalCompares((BigDecimal) null, BigDecimal.ONE);
    }
    
    /**
     * Test that assertBigDecimalCompares_value_null raises failure.
     */
    @Test(expected = ComparisonFailure.class)
    public void assertBigDecimalCompares_BDvalue_null() {
    	AssertBigDecimal.assertBigDecimalCompares(BigDecimal.ONE, null);
    }
    
    /**
     * Test that assertBigDecimalCompares same no failure.
     */
    @Test
    public void assertBigDecimalCompares_BDsame() {
    	AssertBigDecimal.assertBigDecimalCompares(new BigDecimal("0"), BigDecimal.ZERO);
    	AssertBigDecimal.assertBigDecimalCompares(new BigDecimal("1.000"), BigDecimal.ONE);
    	AssertBigDecimal.assertBigDecimalCompares(new BigDecimal("1E1"), BigDecimal.TEN);
    	AssertBigDecimal.assertBigDecimalCompares(new BigDecimal("11E1"), new BigDecimal("110.00"));
    }
    
    /**
     * Test that assertBigDecimalCompares different raises failure.
     */
    @Test
    public void assertBigDecimalCompares_BDdifferent() {
        compareAndCatch(new BigDecimal("0.1"), BigDecimal.ZERO);
        compareAndCatch(new BigDecimal("1.0000001"), BigDecimal.ONE);
        compareAndCatch(new BigDecimal("9.99999999999999999"), BigDecimal.TEN);
    }
    private void compareAndCatch(final String expected, final BigDecimal actual) {
        try {
        	AssertBigDecimal.assertBigDecimalCompares(expected, actual);
        } catch (final ComparisonFailure e) {
            return;
        }
        Assert.fail();
    }
    
    private void compareAndCatch(final BigDecimal expected, final BigDecimal actual) {
        try {
            AssertBigDecimal.assertBigDecimalCompares(expected, actual);
        } catch (final ComparisonFailure e) {
            return;
        }
        Assert.fail();
    }
    


}

