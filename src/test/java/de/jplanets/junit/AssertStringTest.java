package de.jplanets.junit;

import org.junit.Assert;
import org.junit.Test;

import de.jplanets.junit.internal.LinesComparisonFailure;

/**
 * Test the {@link AssertString}.
 * 
 * @since 1.0.0
 *
 */
public class AssertStringTest {
	/**
	 * Test that assertLinesEqual is null save.
	 */
	@Test
	public void assertLinesEqual_NullSave() {
		AssertString.assertLinesEqual(null, null);
		AssertString.assertLinesEqual(null, null, null);
	}

	/**
	 * Test that assertLinesEqual same strings no failure.
	 */
	@Test
	public void assertLinesEqual_Same() {
		// given
		final String[] empty = new String[0];
		final String[] single = new String[] { "1" };
		final String[] multi = new String[] { "1", "", "noch eins" };
		// when
		AssertString.assertLinesEqual(empty, empty);
		AssertString.assertLinesEqual("Single", single, single);
		AssertString.assertLinesEqual(multi, multi);
	}

	/**
	 * Test that assertLinesEqual strings not equal null.
	 */
	@Test
	public void assertLinesEqual_ObjecVsNull() {
		// given
		final String[] empty = new String[0];
		final String[] single = new String[] { "1" };
		final String[] multi = new String[] { "1", "", "noch eins" };
		// when
		compareAndCatch(empty, null);
		compareAndCatch(null, single);
		compareAndCatch(multi, null);
	}

	/**
	 * Test that assertLinesEqual equal strings no failure.
	 */
	@Test
	public void assertLinesEqual_Equal() {
		// given
		final String[] emptyE = new String[0];
		final String[] emptyA = new String[0];
		final String[] singleE = new String[] { "1" };
		final String[] singleA = new String[] { "1" };
		final String[] multiE = new String[] { "1", "", "noch eins" };
		final String[] multiA = new String[] { "1", "", "noch eins" };
		// when
		AssertString.assertLinesEqual(emptyE, emptyA);
		AssertString.assertLinesEqual("Single", singleE, singleA);
		AssertString.assertLinesEqual(multiE, multiA);
	}

	/**
	 * Test that assertLinesEqual different lines by same length collects
	 * failures.
	 */
	@Test
	public void assertLinesEqual_DifferentLinesSameLength() {
		// given
		final String[] singleE = new String[] { "1" };
		final String[] singleA = new String[] { "2" };
		final String[] doubleE = new String[] { "1", "3" };
		final String[] doubleA = new String[] { "1", "4" };
		final String[] multiE = new String[] { "1", "", "noch zwei", "gleich" };
		final String[] multiA = new String[] { "2", "", "noch eins", "gleich" };
		// when
		final LinesComparisonFailure errorSingle = compareAndCatchLinesComparisonFailure(singleE, singleA);
		final LinesComparisonFailure errorDouble = compareAndCatchLinesComparisonFailure(doubleE, doubleA);
		final LinesComparisonFailure errorMulti = compareAndCatchLinesComparisonFailure(multiE, multiA);
		// than
		assertLineEqualMessage(errorSingle.getMessage(), 1);
		assertLineEqualMessage(errorDouble.getMessage(), 2);
		assertLineEqualMessage(errorMulti.getMessage(), 1, 3);
	}

	/**
	 * Test that assertLinesEqual different lines and longer collects failures.
	 */
	@Test
	public void assertLinesEqual_DifferentLinesLonger() {
		// given
		final String[] singleE = new String[] {};
		final String[] singleA = new String[] { "2" };
		final String[] doubleE = new String[] { "1" };
		final String[] doubleA = new String[] { "1", "4" };
		final String[] doubleDiffE = new String[] { "2" };
		final String[] doubleDiffA = new String[] { "1", "4" };
		final String[] multiE = new String[] { "1", "", "gleich" };
		final String[] multiA = new String[] { "2", "", "noch eins", "gleich" };
		// when
		final LinesComparisonFailure errorSingle = compareAndCatchLinesComparisonFailure(singleE, singleA);
		final LinesComparisonFailure errorDouble = compareAndCatchLinesComparisonFailure(doubleE, doubleA);
		final LinesComparisonFailure errorDoubleDiff = compareAndCatchLinesComparisonFailure(doubleDiffE, doubleDiffA);
		final LinesComparisonFailure errorMulti = compareAndCatchLinesComparisonFailure(multiE, multiA);
		// than
		assertLineEqualMessageDiffLength(errorSingle.getMessage(), singleE.length);
		assertLineEqualMessageDiffLength(errorDouble.getMessage(), doubleE.length);
		assertLineEqualMessageDiffLength(errorDoubleDiff.getMessage(), doubleDiffE.length, 1);
		assertLineEqualMessageDiffLength(errorMulti.getMessage(), multiE.length, 1, 3);
	}

	/**
	 * Test that assertLinesEqual different lines and shorter collects failures.
	 */
	@Test
	public void assertLinesEqual_DifferentLinesShorter() {
		// given
		final String[] singleE = new String[] { "2" };
		final String[] singleA = new String[] {};
		final String[] doubleE = new String[] { "1", "4" };
		final String[] doubleA = new String[] { "1" };
		final String[] doubleDiffE = new String[] { "1", "4" };
		final String[] doubleDiffA = new String[] { "2" };
		final String[] multiE = new String[] { "2", "", "noch eins", "gleich" };
		final String[] multiA = new String[] { "1", "", "gleich" };
		// when
		final LinesComparisonFailure errorSingle = compareAndCatchLinesComparisonFailure(singleE, singleA);
		final LinesComparisonFailure errorDouble = compareAndCatchLinesComparisonFailure(doubleE, doubleA);
		final LinesComparisonFailure errorDoubleDiff = compareAndCatchLinesComparisonFailure(doubleDiffE, doubleDiffA);
		final LinesComparisonFailure errorMulti = compareAndCatchLinesComparisonFailure(multiE, multiA);
		// than
		assertLineEqualMessageDiffLength(errorSingle.getMessage(), singleE.length);
		assertLineEqualMessageDiffLength(errorDouble.getMessage(), doubleE.length);
		assertLineEqualMessageDiffLength(errorDoubleDiff.getMessage(), doubleDiffE.length, 1);
		assertLineEqualMessageDiffLength(errorMulti.getMessage(), multiE.length, 1, 3);
	}

	/**
	 * Test that assertLinesEqual cuts off on more than 5 errors.
	 */
	@Test
	public void assertLinesEqual_CutsOff() {
		// given
		final String[] multi5E = new String[] { "1", "2", "3", "4", "5" };
		final String[] multi5A = new String[] { "01", "02", "03", "04", "05" };
		final String[] multi6E = new String[] { "1", "2", "3", "4", "5", "6" };
		final String[] multi6A = new String[] { "01", "02", "03", "04", "05", "06" };
		final String[] multi9E = new String[] { "A", "A", "A", "1", "2", "3", "4", "5", "6" };
		final String[] multi9A = new String[] { "A", "A", "A", "01", "02", "03", "04", "05", "06" };
		final String[] multi7DiffE = new String[] { "1", "2", "3", "4", "5", "6", "7" };
		final String[] multi7DiffA = new String[] { "01", "02", "03", "04", "05", "06" };
		// when
		final LinesComparisonFailure errorMulti5 = compareAndCatchLinesComparisonFailure(multi5E, multi5A);
		final LinesComparisonFailure errorMulti6 = compareAndCatchLinesComparisonFailure(multi6E, multi6A);
		final LinesComparisonFailure errorMulti9 = compareAndCatchLinesComparisonFailure(multi9E, multi9A);
		final LinesComparisonFailure errorMulti7 = compareAndCatchLinesComparisonFailure(multi7DiffE, multi7DiffA);
		// than
		assertLineEqualMessage(errorMulti5.getMessage(), 1, 2, 3, 4, 5);
		assertLineEqualMessage(errorMulti6.getMessage(), 1, 2, 3, 4, 5);
		assertLineEqualMessage(errorMulti9.getMessage(), 4, 5, 6, 7, 8);
		assertLineEqualMessageDiffLength(errorMulti7.getMessage(), multi7DiffE.length, 1, 2, 3, 4, 5);
	}

	/**
	 * Test that assertLinesEqual equal strings no failure.
	 */
	@Test
	public void assertLinesEqual_EqualConfigured() {
		// given
		final String[] emptyE = new String[0];
		final String[] emptyA = new String[0];
		final String[] singleE = new String[] { "1" };
		final String[] singleA = new String[] { "1" };
		final String[] multiE = new String[] { "1", "", "noch eins" };
		final String[] multiA = new String[] { "1", "", "noch eins" };
		// when
		AssertString.assertLinesEqual(emptyE, emptyA, 0);
		AssertString.assertLinesEqual("Single", singleE, singleA, 1);
		AssertString.assertLinesEqual(multiE, multiA, 2);
	}


	/**
	 * Test that assertLinesEqual cuts off on more than given number of differences.
	 */
	@Test
	public void assertLinesEqual_CutsOffConfgured() {
		// given
		final String[] multi5E = new String[] { "1", "2", "3", "4", "5" };
		final String[] multi5A = new String[] { "01", "02", "03", "04", "05" };
		final String[] multi9E = new String[] { "A", "A", "A", "1", "2", "3", "4", "5", "6" };
		final String[] multi9A = new String[] { "A", "A", "A", "01", "02", "03", "04", "05", "06" };
		// when
		final LinesComparisonFailure errorMulti5 = compareAndCatchLinesComparisonFailure(multi5E, multi5A,2);
		final LinesComparisonFailure errorMulti9 = compareAndCatchLinesComparisonFailure(multi9E, multi9A,7);
		// than
		assertLineEqualMessage(errorMulti5.getMessage(), 1, 2);
		assertLineEqualMessage(errorMulti9.getMessage(), 4, 5, 6, 7, 8, 9);
	}

	private LinesComparisonFailure compareAndCatchLinesComparisonFailure(final String[] expecteds,
			final String[] actuals) {
		try {
			AssertString.assertLinesEqual(expecteds, actuals);
		} catch (final LinesComparisonFailure e) {
			return e;
		}
		Assert.fail();
		return null;
	}
	
	private LinesComparisonFailure compareAndCatchLinesComparisonFailure(final String[] expecteds,
			final String[] actuals, int maxDiff) {
		try {
			AssertString.assertLinesEqual(expecteds, actuals, maxDiff);
		} catch (final LinesComparisonFailure e) {
			return e;
		}
		Assert.fail();
		return null;
	}
	
    private void compareAndCatch(final String[] expecteds, final String[] actuals) {
        try {
            AssertString.assertLinesEqual(expecteds, actuals);
        } catch (final AssertionError e) {
            return;
        }
        Assert.fail();
    }


	private void assertLineEqualMessage(String message, int... linesWithError) {
		final String[] actuals = message.split(System.lineSeparator());
		Assert.assertEquals("lines are different at following positions", actuals[0]);
		Assert.assertEquals(linesWithError.length, actuals.length - 1);
		for (int i = 0; i < linesWithError.length; i++) {
			if (!actuals[i + 1].startsWith(String.valueOf(linesWithError[i]))) {
				Assert.fail("'" + actuals[i + 1] + " does not start with '" + String.valueOf(linesWithError[i]) + "'");
			}
		}
	}

	private void assertLineEqualMessageDiffLength(String message, int expectedLines, int... linesWithError) {
		final String[] actuals = message.split(System.lineSeparator());
		if (linesWithError.length == 0) {
			Assert.assertTrue(message.startsWith("expected " + expectedLines + " lines"));
			return;
		}
		Assert.assertEquals("lines are different at following positions", actuals[0]);
		Assert.assertEquals(linesWithError.length, actuals.length - 2);
		for (int i = 0; i < linesWithError.length; i++) {
			if (!actuals[i + 1].startsWith(String.valueOf(linesWithError[i]))) {
				Assert.fail("'" + actuals[i + 1] + " does not start with '" + String.valueOf(linesWithError[i]) + "'");
			}
		}
		Assert.assertTrue(actuals[actuals.length - 1].startsWith("expected " + expectedLines + " lines"));
	}

}
