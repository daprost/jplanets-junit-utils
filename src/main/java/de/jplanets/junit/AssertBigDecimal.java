package de.jplanets.junit;

import java.math.BigDecimal;

import org.junit.ComparisonFailure;
/**
 * A set of special assertions for BigDecimal extending JUnit.
 *
 * @since 1.0.0
 */
public class AssertBigDecimal {
    /**
     * Prevent instantiation of utility class.
     */
    private AssertBigDecimal() throws IllegalAccessException {
        throw new IllegalAccessException();
    }
    /**
     * See {@link AssertBigDecimal#assertBigDecimalCompares(String, String, BigDecimal)} using an empty message.
     *
     * @param expected
     *        the String representation of a BigDecimal
     * @param actual
     *        the value to compare to
     */
    public static void assertBigDecimalCompares(final String expected, final BigDecimal actual) {
        assertBigDecimalCompares("", expected, actual);
    }

    /**
     * Asserts that the BigDecimal representation of the {@code expected} is the same as the {@code actual} based on {@link BigDecimal#compareTo(BigDecimal)}.
     * <br>
     * If both values are {@code null} they no failure is raised.
     *
     * @param message
     *        the message to display on failure
     * @param expected
     *        the String representation of a BigDecimal
     * @param actual
     *        the value to compare to.
     */
    public static void assertBigDecimalCompares(final String message, final String expected, final BigDecimal actual) {
        if ((expected == null) && (actual == null)) {
            return;
        }
        if (expected == null) {
            throw new ComparisonFailure(message, expected, actual.toString());
        }
        if (actual == null) {
            throw new ComparisonFailure(message, expected, null);
        }
        final BigDecimal exp = new BigDecimal(expected);
        if (0 != exp.compareTo(actual)) {
            throw new ComparisonFailure(message, exp.toString(), actual.toString());
        }
    }

    /**
     * See {@link AssertBigDecimal#assertBigDecimalCompares(String, BigDecimal, BigDecimal)} using an empty message.
     *
     * @param expected
     *        the expected value
     * @param actual
     *        the value to compare to
     */
    public static void assertBigDecimalCompares(final BigDecimal expected, final BigDecimal actual) {
        assertBigDecimalCompares("", expected, actual);
    }

    /**
     * Asserts that the {@code expected} is the same as the {@code actual} based on {@link BigDecimal#compareTo(BigDecimal)}.<br>
     * If both values are {@code null} they no failure is raised.
     *
     * @param message
     *        the message to display on failure
     * @param expected
     *        the expected value
     * @param actual
     *        the value to compare to.
     */
    public static void assertBigDecimalCompares(final String message, final BigDecimal expected, final BigDecimal actual) {
        if ((expected == null) && (actual == null)) {
            return;
        }
        if (expected == null) {
            throw new ComparisonFailure(message, null, actual.toString());
        }
        if (actual == null) {
            throw new ComparisonFailure(message, expected.toString(), null);
        }
        if (0 != expected.compareTo(actual)) {
            throw new ComparisonFailure(message, expected.toString(), actual.toString());
        }
    }

}
