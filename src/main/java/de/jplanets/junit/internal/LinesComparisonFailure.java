package de.jplanets.junit.internal;

import java.util.ArrayList;
import java.util.List;

import de.jplanets.junit.AssertString;

/**
 * Wrapper around failures comparing lines (e.g. of files).
 *
 * @see AssertString#assertLinesEqual(String, String[], String[])
 */
public class LinesComparisonFailure extends AssertionError {

	private static final long serialVersionUID = 1L;
	private final List<LineMessage> failureLines;
	private final String headerMessage;
	private int expectedsLength;
	private int actualsLength;

	/**
	 * Create an instance without header message.
	 */
	public LinesComparisonFailure() {
		this(null);
	}

	/**
	 * Create an instance with header message.
	 *
	 * @param headerMessage
	 *            the header message to use for the failure. May be {@code null}
	 *            .
	 */
	public LinesComparisonFailure(String headerMessage) {
		failureLines = new ArrayList<>();
		this.headerMessage = headerMessage;
	}

	/**
	 * Add a failure to the list.
	 *
	 * @param line
	 *            line number of the failure.
	 * @param message
	 *            failure message for the line.
	 */
	public void addFailure(int line, String message) {
		final LineMessage lineMessage = new LineMessage(line, message);
		failureLines.add(lineMessage);
	}

	@Override
	public String getMessage() {
		final StringBuilder builder = new StringBuilder();
		if (headerMessage != null) {
			builder.append(headerMessage);
		}
		if (!failureLines.isEmpty()) {
			builder.append("lines are different at following positions");
			for (final LineMessage lineMessage : failureLines) {
				builder.append(System.lineSeparator());
				builder.append(lineMessage.line);
				builder.append(": ");
				builder.append(lineMessage.message);
			}
			builder.append(System.lineSeparator());
		}
		if (expectedsLength != actualsLength) {
			builder.append("expected " + expectedsLength + " lines; actual are " + actualsLength);
		}
		return builder.toString();
	}

	/**
	 * Set the length of the compared lines.
	 *
	 * @param expectedsLength number of lines expected
	 * @param actualsLength actual number of lines
	 */
	public void setExpectedVsActualSize(int expectedsLength, int actualsLength) {
		this.expectedsLength = expectedsLength;
		this.actualsLength = actualsLength;
	}

	private class LineMessage {

		private final int line;
		private final String message;

		public LineMessage(int line, String message) {
			this.line = line;
			this.message = message;
		}
	}
}
