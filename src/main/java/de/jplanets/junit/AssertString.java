package de.jplanets.junit;

import org.junit.Assert;

import de.jplanets.junit.internal.LinesComparisonFailure;

/**
 * A set of special assertions for Strings extending JUnit.
 *
 * @since 1.0.0
 */
public class AssertString {

	/**
	 * Prevent instantiation of utility class.
	 */
	private AssertString() throws IllegalAccessException {
		throw new IllegalAccessException();
	}

    /**
     * Asserts that two String arrays contain the same Strings. If {@code expected} and
     * {@code actual} are {@code null}, they are considered equal.<br>
     * Records the maximum of 5 differences.
     *
     * @param expecteds array of Strings with expected values
     * @param actuals array of Strings with actual values
     */
    public static void assertLinesEqual(String[] expecteds, String[] actuals) {
        assertLinesEqual(null, expecteds, actuals);
    }

    /**
     * Asserts that two String arrays contain the same Strings. If {@code expected} and
     * {@code actual} are {@code null}, they are considered equal.<br>
     * Records the maximum of 5 differences.
     *
     * @param message the identifying message for the {@link AssertionError} ({@code null}
     *        okay)
     * @param expecteds array of Strings with expected values
     * @param actuals array of Strings with actual values
     */
    public static void assertLinesEqual(String message, String[] expecteds, String[] actuals) {
        internalLinesEqual(message, 5, expecteds, actuals);
    }

    /**
     * Asserts that two String arrays contain the same Strings. If {@code expected} and
     * {@code actual} are {@code null}, they are considered equal.<br>
     *
     * @param expecteds array of Strings with expected values
     * @param actuals array of Strings with actual values
     * @param maxDiffs maximum number of differences to report.
     */
    public static void assertLinesEqual(String[] expecteds, String[] actuals, int maxDiffs) {
        internalLinesEqual(null, maxDiffs, expecteds, actuals);
    }
    
    /**
     * Asserts that two String arrays contain the same Strings. If {@code expected} and
     * {@code actual} are {@code null}, they are considered equal.<br>
     *
     * @param message the identifying message for the {@link AssertionError} ({@code null}
     *        okay)
     * @param expecteds array of Strings with expected values
     * @param actuals array of Strings with actual values
     * @param maxDiffs maximum number of differences to report.
     */
    public static void assertLinesEqual(String message, String[] expecteds, String[] actuals, int maxDiffs) {
        internalLinesEqual(message, maxDiffs, expecteds, actuals);
    }

    private static void internalLinesEqual(String message, int maxDiffs, String[] expecteds, String[] actuals) {
        if (expecteds == actuals) {
            return;
        }
        final String header = message == null ? "" : message + ": ";
        if (expecteds == null) {
            Assert.fail(header + "expected array was null");
        }
        if (actuals == null) {
            Assert.fail(header + "actual array was null");
        }
        final int expectedsLength = expecteds.length;
        final int actualsLength = actuals.length;
        LinesComparisonFailure failure = null;
        if (expectedsLength != actualsLength) {
            failure = new LinesComparisonFailure();
            failure.setExpectedVsActualSize(expectedsLength, actualsLength);
        }
        int diffsCount = 0;
        for (int i = 0; i < expectedsLength; i++) {
            final String expected = expecteds[i];
            if (i >= actualsLength) {
                // skip
            } else {
                final String actual = actuals[i];
                try {
                    // MAYBE faster without try/catch
                    Assert.assertEquals(expected, actual);
                } catch (final AssertionError e) {
                    // collect failure
                    if (failure == null) {
                        failure = new LinesComparisonFailure();
                    }
                    failure.addFailure(i + 1, e.getMessage());
                    diffsCount++;
                    if (diffsCount >= maxDiffs) {
                        break;
                    }
                }
            }
        }
        if (failure != null) {
            throw failure;
        }
    }

}
